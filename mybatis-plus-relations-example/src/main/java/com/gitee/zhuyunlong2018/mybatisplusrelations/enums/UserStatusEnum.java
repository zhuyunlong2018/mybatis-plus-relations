package com.gitee.zhuyunlong2018.mybatisplusrelations.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

@Getter
public enum UserStatusEnum {

    ENABLE(1, "在职"),

    DISABLE(0, "离职"),

    ;

    @EnumValue
    private final Integer value;

    @JsonValue
    private final String desc;

    UserStatusEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }


}
