package com.gitee.zhuyunlong2018.mybatisplusrelations.annotations;

import com.gitee.zhuyunlong2018.mybatisplusrelations.scanner.MyAnnotationBeanRegistrar;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Import(MyAnnotationBeanRegistrar.class)
public @interface RelationScan {

    String[] value() default {};

    String[] basePackages() default {};

    /**
     * 最大的in参数个数，-1不进行循环
     * 设置了值，当in参数大于设定的参数，则将循环拆分为多个子句sql进行拼装
     * @return
     */
    int loopMaxInParams() default -1;
}
