package com.gitee.zhuyunlong2018.mybatisplusrelations.context;

import com.gitee.zhuyunlong2018.mybatisplusrelations.binder.Binder;
import com.gitee.zhuyunlong2018.mybatisplusrelations.func.IGetter;

public class EntityContext<T> extends BaseContext<T> {

    private final T entity;

    @Override
    public T getEntity() {
        return entity;
    }

    public EntityContext(Binder<T> binder, T entity) {
        this.binder = binder;
        this.entity = entity;
        if (null != entity) {
            this.localEntityClass = (Class<T>) entity.getClass();
        }
    }

    /**
     * 是否无用的binder，当主表List为空，或者entity为null的时候，绑定的是无用处理器，不进行操作
     */
    @Override
    public boolean useless(IGetter<T, ?> propertyGetter) {
        if (null != entity) {
            return null == getCache(propertyGetter).getLocalPropertyGetter().apply(entity);
        }
        return true;
    }
}
