package com.gitee.zhuyunlong2018.mybatisplusrelations.exceptions;

public class RelationHandlerException extends RuntimeException {
    public RelationHandlerException(String message) {
        super(message);
    }
}
