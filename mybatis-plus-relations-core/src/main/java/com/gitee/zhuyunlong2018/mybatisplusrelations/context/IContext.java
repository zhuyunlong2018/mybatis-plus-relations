package com.gitee.zhuyunlong2018.mybatisplusrelations.context;

import com.gitee.zhuyunlong2018.mybatisplusrelations.binder.Binder;
import com.gitee.zhuyunlong2018.mybatisplusrelations.cache.RelationCache;
import com.gitee.zhuyunlong2018.mybatisplusrelations.exceptions.RelationHandlerException;
import com.gitee.zhuyunlong2018.mybatisplusrelations.func.IGetter;

import java.util.List;

public interface IContext<T> {

    boolean getIsEnd();

    void setEnd();

    Class<?> getLocalEntityClass();

    Binder<T> getBinder();

    RelationCache getCache(IGetter<T, ?> propertyGetter);

    boolean useless(IGetter<T, ?> propertyGetter);

    default T getEntity() {
        throw new RelationHandlerException("不能获取模型");
    }

    default List<T> getList() {
        throw new RelationHandlerException("不能获取模型list");
    }
}
