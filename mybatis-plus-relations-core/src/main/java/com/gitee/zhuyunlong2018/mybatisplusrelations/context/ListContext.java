package com.gitee.zhuyunlong2018.mybatisplusrelations.context;

import com.gitee.zhuyunlong2018.mybatisplusrelations.binder.Binder;
import com.gitee.zhuyunlong2018.mybatisplusrelations.func.IGetter;

import java.util.List;

public class ListContext<T> extends BaseContext<T> {

    private final List<T> list;

    @Override
    public List<T> getList() {
        return list;
    }

    public ListContext(Binder<T> binder, List<T> list) {
        this.binder = binder;
        this.list = list;
        if (null != list && !list.isEmpty()) {
            T entityFirst = list.get(0);
            localEntityClass = entityFirst.getClass();
        }
    }

    /**
     * 是否无用的binder，当主表List为空，或者entity为null的时候，绑定的是无用处理器，不进行操作
     */
    @Override
    public boolean useless(IGetter<T, ?> propertyGetter) {
        return null == list || list.isEmpty();
    }
}
