package com.gitee.zhuyunlong2018.mybatisplusrelations.handler;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.gitee.zhuyunlong2018.mybatisplusrelations.cache.RelationCache;
import com.gitee.zhuyunlong2018.mybatisplusrelations.binder.IBinder;
import com.gitee.zhuyunlong2018.mybatisplusrelations.context.IContext;
import com.gitee.zhuyunlong2018.mybatisplusrelations.exceptions.RelationAnnotationException;
import com.gitee.zhuyunlong2018.mybatisplusrelations.utils.BeanUtils;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 关联处理器
 *
 * @param <T>
 * @param <R>
 */
public abstract class Handler<T, R> implements IHandler<T, R> {

    /**
     * 关联表list，bindOne也使用list形式进行获取
     */
    protected List<R> foreignEntityList;

    /**
     * 关系构造器
     */
    protected IBinder<T> binder;

    protected IContext<T> context;

    /**
     * 深度绑定构造器
     */
    protected Consumer<IBinder<R>> deepBinderConsumer;

    /**
     * 补充查询器
     */
    protected Consumer<LambdaQueryWrapper<R>> lambdaWrapperFunc;

    /**
     * 中间表补充查询器，多对多的时候使用
     */
    protected Consumer<LambdaQueryWrapper<?>> linkLambdaWrapperFunc;

    /**
     * 关联模型数组的内存过滤器Stream
     */
    protected Predicate<R> streamFilter;

    /**
     * 关联模型数组的内存排序器Stream
     */
    protected Comparator<R> streamComparator;

    /**
     * 关联表是否stream操作
     */
    protected boolean streamStatus;

    /**
     * 中间表模型数组的内存过滤器Stream
     */
    protected Predicate<?> streamLinkFilter;

    /**
     * 中间表模型数组的内存排序器Stream
     */
    protected Comparator<?> streamLinkComparator;

    /**
     * 中间表是否stream操作
     */
    protected boolean streamLinkStatus;

    /**
     * 关联注解关系缓存
     */
    protected RelationCache cache;

    /**
     * 本次处理是否结束
     */
    protected Boolean ended = false;

    /**
     * 初始化关联表query wrapper
     *
     * @return
     */
    @SuppressWarnings({"rawtypes"})
    protected abstract LambdaQueryWrapper<R> getQueryWrapper();

    /**
     * 初始化关联表query wrapper
     *
     * @return
     */
    @SuppressWarnings({"rawtypes"})
    protected LambdaQueryWrapper<R> getQueryWrapper(Collection<?> inParams) {
        return null;
    }

    /**
     * 数据库查询被关联表数据
     */
    protected abstract void queryRelation();

    /**
     * 构建绑定关系
     */
    protected abstract void buildRelation();

    /**
     * 关联副表查询构造器，使用方式如下，其中Dept为关联副表模型
     * .query(wrapper -> wrapper.eq(Dept::getId, 1))
     *
     * @param lambdaWrapperFunc
     * @return
     */
    @Override
    public Handler<T, R> query(Consumer<LambdaQueryWrapper<R>> lambdaWrapperFunc) {
        this.lambdaWrapperFunc = lambdaWrapperFunc;
        return this;
    }

    /**
     * 基于内存的副表内容处理过滤
     *
     * @param streamFilter
     * @return
     */
    @Override
    public Handler<T, R> filter(Predicate<R> streamFilter) {
        this.streamFilter = streamFilter;
        streamStatus = true;
        return this;
    }

    /**
     * 基于内存的副表内容处理排序
     *
     * @param streamComparator
     * @return
     */
    @Override
    public Handler<T, R> sorted(Comparator<R> streamComparator) {
        this.streamComparator = streamComparator;
        streamStatus = true;
        return this;
    }

    /**
     * 获取关联表的stream处理流
     * @return
     */
    protected Stream<R> getForeignStream(List<R> foreignEntityList) {
        Stream<R> stream = foreignEntityList.stream();
        if (streamFilter != null) {
            stream = stream.filter(streamFilter);
        }
        if (streamComparator != null) {
            stream = stream.sorted(streamComparator);
        }
        return stream;
    }

    /**
     * 多对多时候使用
     * 获取中间表的stream处理流
     * @return
     */
    protected Stream<?> getLinkStream(List<?> linkList) {
        Stream<?> stream = linkList.stream();
        if (streamLinkFilter != null) {
            stream = stream.filter((Predicate)streamLinkFilter);
        }
        if (streamLinkComparator != null) {
            stream = stream.sorted((Comparator)streamLinkComparator);
        }
        return stream;
    }

    /**
     * 模型数组转换为VO数组
     * 深度绑定时使用
     *
     * @param modelList
     * @return
     */
    @SuppressWarnings({"unchecked"})
    protected List<R> covertListModelToVO(List<R> modelList) {
        if (null == modelList || modelList.isEmpty()) {
            return modelList;
        }
        if (!cache.getForeignEntityClass().equals(modelList.get(0).getClass())) {
            // 如果关联的是vo，而不是直接关联model，需要转换
            return (List<R>) modelList.stream()
                    .map(e -> {
                        try {
                            Model<?> model = cache.getForeignEntityClass().newInstance();
                            BeanUtils.copyBeanProp(model, e);
                            return model;
                        } catch (InstantiationException | IllegalAccessException instantiationException) {
                            throw new RelationAnnotationException("VO构造函数错误");
                        }
                    })
                    .collect(Collectors.toList());
        }
        return modelList;
    }

    /**
     * 模型数组转换为VO对象
     * 深度绑定时使用
     *
     * @param model
     * @return
     */
    @SuppressWarnings({"unchecked"})
    protected R covertVoToModel(R model) {
        if (null == model) {
            return null;
        }
        if (!cache.getForeignEntityClass().equals(model.getClass())) {
            // 如果关联的是vo，而不是直接关联model，需要转换
            try {
                Model<?> entity = cache.getForeignEntityClass().newInstance();
                BeanUtils.copyBeanProp(entity, model);
                return (R) entity;
            } catch (InstantiationException | IllegalAccessException instantiationException) {
                throw new RelationAnnotationException("VO构造函数错误");
            }
        }
        return model;
    }

    /**
     * 获取被关联表的entity实例
     *
     * @return
     */
    protected Model<?> getForeignModel() {
        Model<?> model = null;
        try {
            model = cache.getForeignEntityClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        assert model != null;
        return model;
    }

    /**
     * 获取中间表model， 多对多时使用
     *
     * @return
     */
    protected Model<?> getLinkModel() {
        Model<?> linkModel = null;
        try {
            linkModel = cache.getLinkEntityClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        // 中间表集合
        assert linkModel != null;
        return linkModel;
    }

    /**
     * 如果需要绑定多个关联属性，这返回binder
     *
     * @return
     */
    public IBinder<T> binder() {
        return binder;
    }

    /**
     * 获取深度绑定器
     *
     * @return
     */
    protected abstract IBinder<R> deepBinder();

    /**
     * 结束关联查询，进行查询
     */
    public void end() {
        if (!ended) {
            ended = true;
            queryRelation();
            buildRelation();
            // 通知binder处理其他handler
            if (!context.getIsEnd()) {
                binder.end();
            }
            // 执行深度绑定操作
            if (deepBinderConsumer != null) {
                IBinder<R> deepBinder = deepBinder();
                if (deepBinder == null) {
                    return;
                }
                deepBinderConsumer.accept(deepBinder);
            }
        }
    }
}
