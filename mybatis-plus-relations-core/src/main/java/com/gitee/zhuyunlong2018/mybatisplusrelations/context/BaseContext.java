package com.gitee.zhuyunlong2018.mybatisplusrelations.context;

import com.gitee.zhuyunlong2018.mybatisplusrelations.binder.Binder;
import com.gitee.zhuyunlong2018.mybatisplusrelations.cache.RelationCache;
import com.gitee.zhuyunlong2018.mybatisplusrelations.func.IGetter;
import com.gitee.zhuyunlong2018.mybatisplusrelations.utils.BeanUtils;

public abstract class BaseContext<T> implements IContext<T> {

    protected boolean isEnd;

    @Override
    public boolean getIsEnd() {
        return isEnd;
    }

    @Override
    public void setEnd() {
        this.isEnd = true;
    }

    /**
     * 主表model类
     */
    protected Class<?> localEntityClass = void.class;

    protected Binder<T> binder;

    @Override
    public Class<?> getLocalEntityClass() {
        return localEntityClass;
    }

    @Override
    public Binder<T> getBinder() {
        return binder;
    }

    @Override
    public RelationCache getCache(IGetter<T, ?> propertyGetter) {
        String propertyName = BeanUtils.convertToFieldName(propertyGetter);
        return RelationCache.getCache(getLocalEntityClass(), propertyName);
    }
}
