package com.gitee.zhuyunlong2018.mybatisplusrelations.handler;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.gitee.zhuyunlong2018.mybatisplusrelations.binder.IBinder;

import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * 无用的处理模块，当绑定对象list为empty或者entity为null的时候使用
 *
 * @param <T>
 * @param <R>
 */
public class UselessHandler<T, R> extends Handler<T, R> implements IManyBindHandler<T, R> {
    public UselessHandler(IBinder<T> binder) {
        this.binder = binder;
    }

    @Override
    protected LambdaQueryWrapper<R> getQueryWrapper() {
        return null;
    }

    @Override
    protected void queryRelation() {
    }

    @Override
    protected void buildRelation() {
    }

    @Override
    public Handler<T, R> query(Consumer<LambdaQueryWrapper<R>> lambdaWrapperFunc) {
        return this;
    }

    @Override
    protected IBinder<R> deepBinder() {
        return null;
    }

    @Override
    public IBinder<T> deepWith(Consumer<IBinder<R>> binderConsumer) {
        return binder;
    }

    /**
     * 设置关联模型数据，设置后，query()方法将失效，并且直接绑定关联模型，不进行数据表检索
     * <p>此方法仅限EntityBindOne使用
     *
     * @param foreignEntity
     */
    @Override
    public IHandler<T, R> setForeignData(R foreignEntity) {
        return this;
    }

    /**
     * 设置关联模型数据，设置后，query()方法将失效，并且直接绑定关联模型，不进行数据表检索
     * <p>此方法为EntityBindMany、ListBindOne、ListBindMany等使用
     *
     * @param foreignList
     */
    public IHandler<T, R> setForeignData(List<R> foreignList) {
        return this;
    }

    @Override
    public void end() {
    }

    @Override
    public <L extends Model<?>> IManyBindHandler<T, R> linkQuery(Consumer<LambdaQueryWrapper<L>> lambdaWrapperFunc) {
        return this;
    }

    @Override
    public <L extends Model<?>> IManyBindHandler<T, R> linkFilter(Predicate<L> streamFilter) {
        return this;
    }

    @Override
    public <L extends Model<?>> IManyBindHandler<T, R> linkSorted(Comparator<L> streamComparator) {
        return this;
    }

    @Override
    public <L> IManyBindHandler<T, R> setLinkData(List<L> linkData) {
        return this;
    }
}
