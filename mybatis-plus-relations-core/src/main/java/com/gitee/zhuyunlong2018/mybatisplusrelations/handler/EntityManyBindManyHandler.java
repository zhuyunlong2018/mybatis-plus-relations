package com.gitee.zhuyunlong2018.mybatisplusrelations.handler;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.gitee.zhuyunlong2018.mybatisplusrelations.config.RelationConfig;
import com.gitee.zhuyunlong2018.mybatisplusrelations.context.IContext;
import com.gitee.zhuyunlong2018.mybatisplusrelations.func.IGetter;
import com.gitee.zhuyunlong2018.mybatisplusrelations.utils.CollectionUtils;
import com.gitee.zhuyunlong2018.mybatisplusrelations.utils.StringUtils;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EntityManyBindManyHandler<T, R> extends EntityBindManyHandler<T, R> implements IManyBindHandler<T, R> {

    /**
     * 连接表的外键集合
     */
    private Set<?> linkForeignProperties;

    /**
     * 中间表数据list
     */
    private List<?> linkList;

    public EntityManyBindManyHandler(IContext<T> context, IGetter<T, ?> propertyGetter) {
        super(context, propertyGetter);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    protected LambdaQueryWrapper<R> getQueryWrapper(Collection<?> inParams) {
        LambdaQueryWrapper<R> wrapper = (LambdaQueryWrapper<R>) Wrappers.lambdaQuery(cache.getForeignEntityClass())
                .in(cache.getForeignPropertyGetter(), inParams);
        wrapper.apply(!StringUtils.isEmpty(cache.getApplySql()), cache.getApplySql());
        if (lambdaWrapperFunc != null) {
            lambdaWrapperFunc.accept(wrapper);
        }
        wrapper.last(!StringUtils.isEmpty(cache.getLastSql()), cache.getLastSql());
        return wrapper;
    }

    @SuppressWarnings({"unchecked"})
    private LambdaQueryWrapper<R> getLinkQueryWrapper() {
        LambdaQueryWrapper<R> linkWrapper = Wrappers.lambdaQuery(cache.getLinkEntityClass())
                .eq(cache.getLinkLocalPropertyGetter(), cache.getLocalPropertyGetter().apply(entity));
        linkWrapper.apply(!StringUtils.isEmpty(cache.getLinkApplySql()), cache.getLinkApplySql());
        if (linkLambdaWrapperFunc != null) {
            linkLambdaWrapperFunc.accept(linkWrapper);
        }
        linkWrapper.last(!StringUtils.isEmpty(cache.getLinkLastSql()), cache.getLinkLastSql());
        return linkWrapper;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected void queryRelation() {
        if (linkList == null) {
            linkList = getLinkModel().selectList((LambdaQueryWrapper) getLinkQueryWrapper());
        }
        if (streamLinkStatus) {
            // 过滤处理
            linkList = getLinkStream(linkList).collect(Collectors.toList());
        }
        // 外键集合ID
        linkForeignProperties = (Set<?>) linkList
                .stream()
                .map(cache.getLinkForeignPropertyGetter())
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        if (!linkForeignProperties.isEmpty() && foreignEntityList == null) {
            if (RelationConfig.shouldLoop(linkForeignProperties)) {
                // 将关联的in参数进行分割成多个
                Collection<? extends Collection<?>> collections = CollectionUtils.subCollection(linkForeignProperties, RelationConfig.LOOP_MAX_IN_PARAMS);
                foreignEntityList = new ArrayList<>();
                for (Collection<?> collection : collections) {
                    if (collection.isEmpty()) {
                        break;
                    }
                    foreignEntityList.addAll(
                            (List<R>) getForeignModel()
                                    .selectList((LambdaQueryWrapper) getQueryWrapper(collection))
                    );
                }
            } else {
                foreignEntityList = (List<R>) getForeignModel()
                        .selectList((LambdaQueryWrapper) getQueryWrapper(linkForeignProperties));
            }
            // 换行VO
            foreignEntityList = covertListModelToVO(foreignEntityList);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected void buildRelation() {
        if (linkList.isEmpty() || foreignEntityList == null || foreignEntityList.isEmpty()) {
            cache.getRelationEntitySetter().accept(entity, Collections.emptyList());
            return;
        }
        cache.getRelationEntitySetter().accept(entity, foreignEntityList);
        if (!StringUtils.isEmpty(cache.getIterateLinkMethod())) {
            // 进入模型迭代器
            cache.getIterateLinkMethodSetter().accept(entity, linkList);
        }
        if (streamStatus) {
            Stream<R> stream = getForeignStream(foreignEntityList);
            foreignEntityList = stream.collect(Collectors.toList());
            cache.getRelationEntitySetter().accept(entity, foreignEntityList);
        }
    }

    /**
     * 中间表查询构造，多对多时才有用，使用方式如下，其中UserSkillRelation为中间表模型
     * .linkQuery((LambdaQueryWrapper<UserSkillRelation> wrapper) -> {
     * wrapper.gt(UserSkillRelation::getScore, 90);
     * })
     *
     * @param lambdaWrapperFunc
     * @param <L>
     * @return
     */
    @SuppressWarnings({"unchecked"})
    public <L extends Model<?>> IManyBindHandler<T, R> linkQuery(Consumer<LambdaQueryWrapper<L>> lambdaWrapperFunc) {
        this.linkLambdaWrapperFunc = (Consumer<LambdaQueryWrapper<?>>) (Object) lambdaWrapperFunc;
        return this;
    }

    @Override
    public <L extends Model<?>> IManyBindHandler<T, R> linkFilter(Predicate<L> streamFilter) {
        streamLinkFilter = streamFilter;
        streamLinkStatus = true;
        return this;
    }

    @Override
    public <L extends Model<?>> IManyBindHandler<T, R> linkSorted(Comparator<L> streamComparator) {
        streamLinkComparator = streamComparator;
        streamLinkStatus = true;
        return this;
    }

    @Override
    public <L> IManyBindHandler<T, R> setLinkData(List<L> linkData) {
        linkList = linkData;
        return this;
    }
}
