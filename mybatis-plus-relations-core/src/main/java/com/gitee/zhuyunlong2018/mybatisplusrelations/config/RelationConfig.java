package com.gitee.zhuyunlong2018.mybatisplusrelations.config;

import java.util.Collection;

public class RelationConfig {

    /**
     * 最大的in参数个数，-1不进行循环
     * 设置了值，当in参数大于设定的参数，则将循环拆分为多个子句sql进行拼装
     */
    public static int LOOP_MAX_IN_PARAMS;

    /**
     * 是否需要进行轮训子查询
     * @param params
     * @return
     */
    public static Boolean shouldLoop(Collection<?> params) {
        return LOOP_MAX_IN_PARAMS > 0 && params.size() > LOOP_MAX_IN_PARAMS;
    }
}
