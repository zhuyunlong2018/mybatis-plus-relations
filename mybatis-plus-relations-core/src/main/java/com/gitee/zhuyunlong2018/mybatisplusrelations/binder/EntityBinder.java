package com.gitee.zhuyunlong2018.mybatisplusrelations.binder;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.gitee.zhuyunlong2018.mybatisplusrelations.context.EntityContext;
import com.gitee.zhuyunlong2018.mybatisplusrelations.handler.*;
import com.gitee.zhuyunlong2018.mybatisplusrelations.func.IGetter;


import java.util.List;

/**
 * 主表单独entity绑定器
 *
 * @param <T>
 */
public class EntityBinder<T> extends Binder<T> {

    public EntityBinder(T entity) {
        context = new EntityContext<>(this, entity);
    }

    /**
     * 绑定多对多关系
     *
     * @param propertyGetter
     * @param <R>
     * @return
     */
    @Override
    public <R extends Model<?>> IManyBindHandler<T, R> manyBindMany(IGetter<T, List<R>> propertyGetter) {
        if (context.useless(propertyGetter)) {
            return (IManyBindHandler<T, R>) useLessBinder();
        }
        EntityManyBindManyHandler<T, R> handler = new EntityManyBindManyHandler<>(context, propertyGetter);
        handlers.add(handler);
        return handler;
    }

    /**
     * 绑定一对多关系
     *
     * @param propertyGetter
     * @param <R>
     * @return
     */
    @Override
    public <R extends Model<?>> IHandler<T, R> bindMany(IGetter<T, List<R>> propertyGetter) {
        if (context.useless(propertyGetter)) {
            return useLessBinder();
        }
        EntityBindManyHandler<T, R> handler = new EntityBindManyHandler<>(context, propertyGetter);
        handlers.add(handler);
        return handler;
    }

    /**
     * 绑定一对一关系
     *
     * @param propertyGetter
     * @param <R>
     * @return
     */
    @Override
    public <R extends Model<?>> IHandler<T, R> bindOne(IGetter<T, R> propertyGetter) {
        if (context.useless(propertyGetter)) {
            return useLessBinder();
        }
        EntityBindOneHandler<T, R> handler = new EntityBindOneHandler<>(context, propertyGetter);
        handlers.add(handler);
        return handler;
    }
}
